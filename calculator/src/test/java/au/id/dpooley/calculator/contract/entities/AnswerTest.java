package au.id.dpooley.calculator.contract.entities;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

class AnswerTest {
	
	private Answer answer;

	@Test
	void shouldReturnValueSetInConstructor() {
		answer = new Answer(new BigDecimal(10));
		
		assertEquals(10, answer.getValue().intValue());
	}
}
