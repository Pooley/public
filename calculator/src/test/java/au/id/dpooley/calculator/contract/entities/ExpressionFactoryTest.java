package au.id.dpooley.calculator.contract.entities;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import au.id.dpooley.calculator.operations.Addition;
import au.id.dpooley.calculator.operations.Division;
import au.id.dpooley.calculator.operations.Multiplication;
import au.id.dpooley.calculator.operations.Subtraction;

class ExpressionFactoryTest {

	@Test
	void shouldReturnAdditionOperationWhenOperandIsADDITON() {
		assertEquals(Addition.class, ExpressionFactory.createOperation(Operand.ADDITION).getClass());
	}
	
	@Test
	void shouldReturnSubtractionOperationWhenOperandIsSUBTRACTION() {
		assertEquals(Subtraction.class, ExpressionFactory.createOperation(Operand.SUBTRACTION).getClass());
	}
	
	@Test
	void shouldReturnAdditionOperationWhenOperandIsMULTIPLICATION() {
		assertEquals(Multiplication.class, ExpressionFactory.createOperation(Operand.MULTIPLICATION).getClass());
	}
	
	@Test
	void shouldReturnAdditionOperationWhenOperandIsDIVISION() {
		assertEquals(Division.class, ExpressionFactory.createOperation(Operand.DIVISION).getClass());
	}
}
