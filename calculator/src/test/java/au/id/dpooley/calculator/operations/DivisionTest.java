package au.id.dpooley.calculator.operations;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import au.id.dpooley.calculator.contract.entities.Answer;

public class DivisionTest extends OperationArgumentVerifierTest {

	@BeforeEach
	public void setup() {
		operation = new Division();
	}

	@Test
	void shouldReturn7WhenMultiplying28By4() {
		Answer answer = operation.solve(new BigDecimal(28), new BigDecimal(4));
		
		assertEquals(7, answer.value().intValue());
	}
}
