package au.id.dpooley.calculator.operations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

public abstract class OperationArgumentVerifierTest {
	
	protected Operation operation;

	@Test
	void shouldThrowIllegalArguementExceptionWhenLHSArgumentIsNull() {
		Exception exception = assertThrows(IllegalArgumentException.class, ()-> operation.solve(null, new BigDecimal(7)));
	
		assertEquals("LHS argument is null", exception.getMessage());
	}
	
	@Test
	void shouldThrowIllegalArguementExceptionWhenRHSArgumentIsNull() {
		Exception exception = assertThrows(IllegalArgumentException.class, ()-> operation.solve(new BigDecimal(7), null));
	
		assertEquals("RHS argument is null", exception.getMessage());
	}
	
	@Test
	void shouldThrowIllegalArguementExceptionWhenBothArgumentsAreNull() {
		Exception exception = assertThrows(IllegalArgumentException.class, ()-> operation.solve(null, null));
	
		assertEquals("Both arguments are null", exception.getMessage());
	}
}
