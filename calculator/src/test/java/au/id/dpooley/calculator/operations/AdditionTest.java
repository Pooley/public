package au.id.dpooley.calculator.operations;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import au.id.dpooley.calculator.contract.entities.Answer;

class AdditionTest extends OperationArgumentVerifierTest {
	
	@BeforeEach
	public void setup() {
		operation = new Addition();
	}

	@Test
	void shouldReturn11WhenAdding4And7() {
		Answer answer = operation.solve(new BigDecimal(4), new BigDecimal(7));
		
		assertEquals(11, answer.value().intValue());
	}
}
