package au.id.dpooley.calculator.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import au.id.dpooley.calculator.contract.entities.Answer;
import au.id.dpooley.calculator.contract.entities.Operand;

@SpringBootTest
public class CalculatorServiceTest {
	
	@Autowired
	private CalculatorService calculatorService;
	
	@Test
	public void shouldDoSimpleAdditionOfAnySetOfNumbers() {
		Answer answer = calculatorService.calculate(Operand.ADDITION, new BigDecimal[] {new BigDecimal(1),new BigDecimal(2),new BigDecimal(3)});
		
		assertEquals(6, answer.getValue().intValue());
	}
	
	@Test
	public void shouldDoSimpleSubtractionOfAnySetOfNumbers() {
		Answer answer = calculatorService.calculate(Operand.SUBTRACTION, new BigDecimal[] {new BigDecimal(10),new BigDecimal(2),new BigDecimal(3)});
		
		assertEquals(5, answer.getValue().intValue());
	}
	
	@Test
	public void shouldDoSimpleMultiplicationOfAnySetOfNumbers() {
		Answer answer = calculatorService.calculate(Operand.MULTIPLICATION, new BigDecimal[] {new BigDecimal(10),new BigDecimal(2),new BigDecimal(3)});
		
		assertEquals(60, answer.getValue().intValue());
	}
	
	@Test
	public void shouldDoSimpleDivisionOfAnySetOfNumbers() {
		Answer answer = calculatorService.calculate(Operand.DIVISION, new BigDecimal[] {new BigDecimal(10),new BigDecimal(2)});
		
		assertEquals(5, answer.getValue().intValue());
	}
}
