package au.id.dpooley.calculator.operations;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import au.id.dpooley.calculator.contract.entities.Answer;

public class SubtractionTest extends OperationArgumentVerifierTest {

	@BeforeEach
	public void setup() {
		operation = new Subtraction();
	}
	
	@Test
	void shouldReturn3WhenSubtracting4From7() {
		Answer answer = operation.solve(new BigDecimal(7), new BigDecimal(4));
		
		assertEquals(3, answer.value().intValue());
	}
}
