Feature: combination of  + - * / 

	Background:
		GIVEN the calculator is running

	Scenario: subtract two numbers from each other
		WHEN the calculator is asked to subtract 4 from 7
		AND the calculator is asked to multiply the answer by 4
		AND the calculator is asked to divide the answer by 3
		AND the calculator is asked to add the answer to 14
		THEN the answer is 18