Feature: subtraction

	Background:
		GIVEN the calculator is running

	Scenario: subtract two numbers from each other
		WHEN the calculator is asked to subtract 4 from 7
		THEN the answer is 3
	
	Scenario Outline: add any number of numbers together
		WHEN the calculator subtracts the numbers <numbers>
		THEN the answer is <answer>
		
		Examples:
		| numbers | answer |
		|	2,1			|	1			 |
		|	11,4,2	|	5			 |
		|	10,3,5,2|	0			 |
	
	Scenario Outline: one or more input is not a number
		WHEN the calculator subtracts the numbers <numbers>
		THEN the error message is "<error> is not a number"
		
		Examples:
		| numbers | error	 |
		|	2,a			|	a			 |
		|	b,4,2		|	b			 |
		|	10,c,d,2|	c,d		 |