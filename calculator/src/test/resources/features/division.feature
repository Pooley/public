Feature: division

	Background:
		GIVEN the calculator is running

	Scenario: divide two numbers from each other
		WHEN the calculator is asked to divide 28 by 7
		THEN the answer is 4
	
	Scenario Outline: divide any number of numbers together
		WHEN the calculator divides the numbers <numbers>
		THEN the answer is <answer>
		
		Examples:
		| numbers | answer |
		|	2,1			|	1			 |
		|	12,3,2	|	2			 |
		|	10,1,5,0|	0			 |
	
	Scenario Outline: one or more input is not a number
		WHEN the calculator subtracts the numbers <numbers>
		THEN the error message is "<error> is not a number"
		
		Examples:
		| numbers | error	 |
		|	2,a			|	a			 |
		|	b,4,2		|	b			 |
		|	10,c,d,2|	c,d		 |