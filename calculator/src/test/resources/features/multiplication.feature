Feature: multiplication

	Background:
		GIVEN the calculator is running

	Scenario: multiply two numbers together
		WHEN the calculator is asked to multiply 4 and 7
		THEN the answer is 28
	
	Scenario Outline: multiply any number of numbers together
		WHEN the calculator multiplies the numbers <numbers>
		THEN the answer is <answer>
		
		Examples:
		| numbers | answer |
		|	2,1			|	2			 |
		|	11,4,2	|	88		 |
		|	10,3,5,0|	0			 |
	
	Scenario Outline: one or more input is not a number
		WHEN the calculator multiplies the numbers <numbers>
		THEN the error message is "<error> is not a number"
		
		Examples:
		| numbers | error	 |
		|	2,a			|	a			 |
		|	b,4,2		|	b			 |
		|	10,c,d,2|	c,d		 |