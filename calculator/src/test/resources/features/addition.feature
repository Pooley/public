Feature: Addition

	Background:
		GIVEN the calculator is running

	Scenario: add two numbers together
		WHEN the calculator is asked to add 4 and 7
		THEN the answer is 11
	
	Scenario Outline: add any number of numbers together
		WHEN the calculator adds the numbers <numbers>
		THEN the answer is <answer>
		
		Examples:
		| numbers | answer |
		|	1,2			|	3			 |
		|	1,2,3		|	6			 |
		|	1,2,3,4	|	10		 |
	
	Scenario Outline: one or more input is not a number
		WHEN the calculator adds the numbers <numbers>
		THEN the error message is "<error> is not a number"
		
		Examples:
		| numbers | error	 |
		|	1,a			|	a 		 |
		|	a,2,3		|	a			 |
		|	1,2,c,4	|	c			 |