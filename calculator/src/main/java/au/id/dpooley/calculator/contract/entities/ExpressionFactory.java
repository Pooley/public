package au.id.dpooley.calculator.contract.entities;

import java.util.HashMap;

import au.id.dpooley.calculator.operations.Addition;
import au.id.dpooley.calculator.operations.Division;
import au.id.dpooley.calculator.operations.Multiplication;
import au.id.dpooley.calculator.operations.Operation;
import au.id.dpooley.calculator.operations.Subtraction;

public class ExpressionFactory {
	
	private static HashMap<Operand, Operation> operandOperationMap = new HashMap<>();
	
	static {
		operandOperationMap.put(Operand.ADDITION, new Addition());
		operandOperationMap.put(Operand.SUBTRACTION, new Subtraction());
		operandOperationMap.put(Operand.MULTIPLICATION, new Multiplication());
		operandOperationMap.put(Operand.DIVISION, new Division());
	}
	
	public static Operation createOperation(Operand operand) {
		return operandOperationMap.get(operand);
	}
}
