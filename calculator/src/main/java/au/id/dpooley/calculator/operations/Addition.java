package au.id.dpooley.calculator.operations;

import java.math.BigDecimal;

public class Addition extends OperationArguementVerifier {

	@Override
	BigDecimal solveIt(BigDecimal lhs, BigDecimal rhs) {
		System.out.println(lhs + " " + rhs);
		return lhs.add(rhs);
	}
}