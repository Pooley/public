package au.id.dpooley.calculator.contract.entities;

import java.math.BigDecimal;

 public class Answer {
	private BigDecimal value;

	Answer(BigDecimal value) {
		this.value = value;
	}

	public BigDecimal value() {
		return value;
	}

	public BigDecimal getValue() {
		return value;
	}
}
