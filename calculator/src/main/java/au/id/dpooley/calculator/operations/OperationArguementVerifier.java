package au.id.dpooley.calculator.operations;

import java.math.BigDecimal;

import au.id.dpooley.calculator.contract.entities.Answer;
import au.id.dpooley.calculator.contract.entities.AnswerFactory;

public abstract class OperationArguementVerifier implements Operation {
	
	@Override
	public Answer solve(BigDecimal lhs, BigDecimal rhs) {
		this.verify(lhs, rhs);
		
		BigDecimal total = this.solveIt(lhs, rhs);
		
		Answer answer = AnswerFactory.create(total);
		
		return answer;
	}
	
	abstract BigDecimal solveIt(BigDecimal lhs, BigDecimal rhs);

	private void verify(BigDecimal lhs, BigDecimal rhs) {
		if (lhs == null && rhs == null) {
			throw new IllegalArgumentException("Both arguments are null");
		}
		
		if (lhs == null) {
			throw new IllegalArgumentException("LHS argument is null");
		}
		
		if (rhs == null) {
			throw new IllegalArgumentException("RHS argument is null");
		}
	}
}
