package au.id.dpooley.calculator.operations;

import java.math.BigDecimal;

import au.id.dpooley.calculator.contract.entities.Answer;

public interface Operation {

	Answer solve(BigDecimal lhs, BigDecimal rhs);
	
}
