package au.id.dpooley.calculator.operations;

import java.math.BigDecimal;

public class Division extends OperationArguementVerifier {

	@Override
	BigDecimal solveIt(BigDecimal lhs, BigDecimal rhs) {
		return lhs.divide(rhs);
	}
}
