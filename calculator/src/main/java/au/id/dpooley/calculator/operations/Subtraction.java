package au.id.dpooley.calculator.operations;

import java.math.BigDecimal;

public class Subtraction extends OperationArguementVerifier {

	@Override
	BigDecimal solveIt(BigDecimal lhs, BigDecimal rhs) {
		return lhs.subtract(rhs);
	}
}
