package au.id.dpooley.calculator.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import au.id.dpooley.calculator.contract.entities.Answer;
import au.id.dpooley.calculator.contract.entities.ExpressionFactory;
import au.id.dpooley.calculator.contract.entities.Operand;

import au.id.dpooley.calculator.operations.Operation;

@Service
public class CalculatorService {

	public Answer calculate(Operand operand, BigDecimal[] numbers) {
		Answer returns = null;
		
		Operation operation = ExpressionFactory.createOperation(operand);
		
		for (int i=0; i < numbers.length; i++) {
			if (returns == null) {
				returns = operation.solve(numbers[i], numbers[i+1]);
			}
			else {
				returns = operation.solve(returns.getValue(), numbers[i]);
			}
			i++;
		}
		
		return returns;
	}
}
