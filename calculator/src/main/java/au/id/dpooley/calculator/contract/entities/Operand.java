package au.id.dpooley.calculator.contract.entities;

public enum Operand {
	ADDITION("+"),
	SUBTRACTION("-"),
	MULTIPLICATION("*"),
	DIVISION("/");

	Operand(String operandAsString) {
	}
}
