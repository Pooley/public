package au.id.dpooley.calculator.contract.entities;

import java.math.BigDecimal;

public class AnswerFactory {

	public static Answer create(BigDecimal value) {
		return new Answer(value);
	}
}
