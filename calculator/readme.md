# Calculator Microservice Reference

The task to design and proto-code a Calculator Microservice is my form of technical interview. The goal is for the candidate to show how they think about a service’s API and method level contracts; how well they know and can apply Design Patterns and utilise Object Orientated Principles. The task isn’t to find out how well they can make a calculator.

This project is to not only provide my view of the above but also the fundamental software development principles that any good software developer should do when they are developing and maintaining their - and someone else's - codebase.

## Documentation

The very first thing a developer or team should do is create a space where they can write down their thoughts, scratch out a design, and over time as each of the practices and tasks below are completed, turn the space into a formal service document. The objective of documentation is to be the Quick Reference point for anybody to understand:

- What the system is and does
- Where the code and artefact repositories are (as hyperlinks).
- Where the environments are (as hyperlinks).
- Where the monitoring alerts are (as hyperlinks).
- Where the build jobs are (as hyperlinks).
- Who the contacts points are (email, instant messaging).
- What its roadmap is (list of JIRA cards and link to JIRA project).
- Its risk and issue registry and decisions taken.
- What its Contracts are (link to OpenAPI spec).
- What its integrations are (described in UML) and links to those spaces.
- What its datamodel is (described in UML).

without having to go into the code and read dozens to hundreds of files.

## Base Project

### Gitlab
This project his hosted at: [https://gitlab.com/Pooley/public/calculator](https://gitlab.com/Pooley/public/calculator)

### Java
Java is the Enterprise development language. Other languages have their place in certain parts of the stack but as an ecosystem of frameworks and tried and tested Open Source libraries Java cannot be beat.

### Gradle
Normally I would use Maven - but decided to use Gradle again even though I find its documentation and community far more burdensome to get to what I need than Maven.

### Springboot
Spring took over from J2EE in the late 2000’s and despite Red Hat and Oracle’s attempts to return the public EE 3 with Contextual Dependency Injection (CDI, which imho was superior to Spring) it failed and Oracle has given up. Hence Spring remains the community enterprise standard; when building enterprise grade software, it is best to stick to standards.

## Continuous Integration
It must seem strange to set up CI before writing any code. However I believe that foundational layers must be applied first as solid foundations ensure that the system will be less likely to fall over.

### Containerization
#### Docker
Docker is the community standard in containerization. Only a simple Dockerfile is required as the Continuous Delivery system will use Config as Code to apply environmental runtime configuration to the system.

Containers give us the ability to build a system once and move it across environments, applying environment-level configuration as part of automated CD pipelines instead of having to manage the difficulty in environment-level branches by constantly merging code and remembering to apply bug fixes correctly.

### Artifact Repository
Once something has been built it should be stored somewhere so it does not have to be built again and is available for other tools and systems to access easily. Repositories store:

- Dependency libraries for your own applications whether 3rd party or self built. 
- Containers to ship across environments.

Nexus and Artifactory are two main packaged repositories and many git SaaS providers have their own as well. Gitlab for example although it requires a premium account to use it. The Calculator uses Artifactory.

#### JFrog Artifactory
In addition to storing the built Calculator packages, at the beginning we want to use Artifactory to be a central store for all the library dependencies that the Calculator uses such as the Spring Framework.

The Calculator’s build.gradle repositories section references Artifactory instead of mavenCentral() and a remote repository is configured in Artifactory to point to mavenCentral instead.

The artifactoryPublish task pushes the Calculator jar to Artifactory’s libs-release-local repository. If the Calculator could be used as an embedded jar (which is public java method contracts can) then another system can have the Calculator as a dependency without having to download and build the system locally to stay up to date.

### Code Check
Human eyes are never enough to catch all the complexities, anti-patterns, and security vulnerabilities that a team of developers can introduce into a codebase. Like so many other stages in the SDLC, checking the code for cleanliness and quality should be automated.

#### Cobertura/Jacoco
Calculates test coverage by class, method and line, importantly highlighting is not being covered (hint: this is where you should immediately go and write new tests).

There is no reason for a codebase not to have 80%+ coverage; and with dependency mocking and following TDD it is not hard to be at 100% either (and probably higher as tests will call the same lines multiple times). Developers who complain about testing and use strawman arguments regarding getters/setters - which would be taken care of either by code in any case (if the getters/setters aren’t being used then then they can be removed) - are not interested in being good at their craft.

#### Checkstyle
Analysis code for unused imports, empty blocks, duplicated code, method size and complexity (for example many nested loop and if statements) that would make code difficult to understand and to test well, the adherence of naming and format standards that an organisation has.

#### Sonarqube
Sonar is a system that wraps many Code Check topics under a single roof for all projects with the added advantage of being able to differentiate new changes against old which is very useful to monitor quality over time; as scans are run against merges, Blames are easy to identify.

### CI
Continuous Integration has been with us for nearly two decades as an easy and reliable way to trigger the build, package, test and script changes of a source control repository. CI keeps us honest, making sure that the changes we make will be verified (hopefully before) anyone else finds out that there is a simple problem (like it doesn’t compile or a test fails). CI also executes the commands to put the built package into the Artifact Repository.

#### Pipeline as Code
Just as application code is stored in version control, the build scripts should be as well for all the same reasons. The build scripts can be modified by hand or in the CI server and exported.

#### GoCD
Jenkins is the warhorse of the CI world; instead I will use GoCD from Thoughtworks.

A Developer should always run their build locally before pushing it into their branch in the code repository - which is a good time to take a break - and then if it's ready open the Pull Request as their probably will not be a build job for each branch (it would be possible if fully automated -  the creation of the CI pipeline to its destruction when the branch was closed).

When the Pull Request is approved (and anyone in the team should be allowed to make comments, and preferably more than one pair of eyes for an approval to merge) the merge triggers the GoCD build pipeline which:

- Cleans up the agent pipeline repository.
- Builds the system using gradle.
- Runs the BDD Acceptance Criteria.
- Publishes the built jar to the Artifact Repository.
- Tells SonarCube to scan the source code.

GoCD (below) uses Config Repositories to bind version controlled pipeline files to the GoCD server.

### Acceptance Criteria
The project is set up, the build and quality checks are in and automated - it must be time to start writing the code. Not quite. Before code can be written the requirements - what the Calculator or parts of it - is meant to do, have to be defined.

I have found requirements documents to - unless written by an experienced technical writer - be written for the writer’s understanding and not the audience. If there is ever a way to misunderstand what someone has written, need to ask questions about it, or have a different opinion than someone else, then it is when the writer does not write for their audience.

Unless, then, you have a team of quality technical writers on hand to author unambiguous requirements documents, then it is best to write requirements as Acceptance criteria and do it in as simple a format as possible.

#### Behaviour Driven Development
BDD is the answer to misunderstandings and differing opinions of requirements. It is a simple, clearer, and shorter way to state what should happen when something happens.

Just as Test Driven Development (TDD, below) is the practice of writing unit tests before writing the implementation code, BDD is the practice of writing integration level and UI tests (generally) BEFORE writing anything else. Combined, they both create the Regression suite that protects a codebase from accidents and to assist refactoring from breaking previously working behaviour (functionality).

##### Gherkin
BDD uses three keywords:

```
GIVEN: a starting point
WHEN: something happens
THEN: the outcome is verified
```

for authors to write clear, step by step, based Acceptance Criteria. An Acceptance Criteria should be short and only cover a single scenario/use case; a collection of Criteria make up a Feature.

```
Feature: Addition
Scenario: successful addition of two numbers
GIVEN the calculator service is running
	WHEN the calculator is asked to add 4 and 7
	THEN the answer is 11

Scenario: one input is not a number
GIVEN the calculator service is running
	WHEN the calculator is asked to add 4 and Z
	THEN the answer is “Z is not a number”
```

Positive and negative (especially) scenarios must be in every Feature.

What will quickly become apparent is that the same statements will appear scenario after scenario - a library in effect. Publishing the library ensures its reuse so the common problem of similar statements such as

```
WHEN the calculator adds the numbers “4,7”
```

don’t pollute the library with confusing variations. The library also allows authors to quickly write scenarios with only needing to change the parameters by copying and pasting the statements they need from the library.

##### Cucumber
There may be BDD framework alternatives such as Spock but I will always stick with Cucumber. 

Cucumber binds Acceptance Criteria to the code that executes the steps. The important word here is code. Regular whatever-language-code that a developer writes. The important word here is developer.

Developers implement the Acceptance Criteria as code - they write the tests. They author their own scenarios to cover gaps - usually negative cases that were missed during Story Grooming.

Automation Testers I hear you cry. Either you can code, and if you can you should be a developer and not a Tester, and if you can’t, then you are not going to be able to create good test cases that will need to assert the UI, APIs, databases, log files, servers, etc. all the stuff that must be tested.

In short, regular Testers don’t have the training to write the Regression Suite and integration tests. Only people who can program can. Good Testers break things instead.

##### Selenium
Before BDD there was Selenium, and after BDD there is still Selenium as the underlying testing framework.

### Solution Design
Now that all of the foundational supporting infrastructure and requirements are in place it is time to start development and the first phase is to define the contract that a system exposes and dig a little bit into its inner workings. In the Calculator's case it is its RESTful APIs when operating as a standalone microservice, and Java methods when it is an embedded jar dependency.

#### Contract First Development
Just as Acceptance Criteria is defined before development, and unit tests are written before implementation code (below), the contract of a system - its input operations and schema, and output, need to be well thought about upfront.

Why, even though we know that it will probably be subject to change? There are a few reasons:

- It is part of the design process.
- There will be dependent systems and teams of the Calculator; they don’t need a fully working system to integrate with right away but they do need a contract specification to work against (that they can mock).
- Coding first will result in a contract that will fit the code, be more volatile as the code is refactored and be less in line with the need of the requirements.

##### OpenAPI 3.0 Specification
After a no-struggle with MuleSoft’s RAML, Swagger, formalised to OpenAPI, has become the community standard for defining RESTful API contracts. While it is not ‘enforceable’ like RAML’s framework is, there are plenty of code generators to build clients and even the server side as well.

##### Method Specification

#### Unified Modeling Language

UML is a standardised, visual, way to explain how software hangs together, big or small. Depending on your role you will use different diagrams more than others. UML diagrams will be embedded into the documentation (above). I have seen companies put their UML in version control with I suppose the purpose of tracking when changes happened although I have not seem the same places actually treat it that way in practice.

##### Sequence Diagram

The 

##### Class & Entity Diagram

The Calculator does not deal with persisting data and so does not need any ER modelling or NoSQL document schema. It does require Entities for the API input and output. The OpenAPI specification's Schema section also have to be turned into Java objects. This can be done with codegen tools removing some boilerplate development and to keep the two worlds in sync with each other.

### Coding

#### Self Documenting Code

#### Test Driven Development

##### Unit Tests

###### Mocking

#### Logging

##### Log Levels

- TRACE
- DEBUG
- INFO
- WARN
- ERROR
- FATAL

##### Structured Log Events
Log messages should be written as key/value pairs. This will make the job of searching for keywords and creating alerts and dashboards (see Monitoring & Observability) much easier and faster - and when trouble hits the production fan you will be thankful that you took the effort upfront to design your log event contract and data structure.

- Service name
- Process name
- Trace-id
- Timestamp
- Log level
- Request
- Response

#### Performance Driven Development
Good logs enable Observability (below) which will gather metrics on the performance of the service. Business or Non-functional requirements must specify the performance SLA of the service response time even if in part of a larger business process.

Optimisation - CPU and memory usage etc. is also part of performance and directly impacts (cloud) compute resource costs. Use dependency analyers to remove 3rd part libraries that are not actually used; garbage collector metrics by letting the service run for a long time to see if there are memory leaks; performance tests whilst outputting method level timings to see where most processing time is taking place - rewritting the content of the method to use faster libraries or optimise lines - and testing again. Too often performance is left near to production instead of taken into hand at the beginning.

#### Object Orientated Principles

##### Inheritance

##### Polymorphism

##### Encapsulation

##### Abstraction

#### Design Patterns

##### Factory

##### Strategy

##### Command

##### Visitor

##### Recursion

## Monitoring & Observability

### Prometheus

### Grafana

### DataDog

### Micrometer

### ELK

#### ElasticSearch

#### LogStash

#### Kibana

### Continuous Delivery

#### GoCD

### Container Orchestration

#### Kubernetes

##### Kops

##### Helm

#### Service Mesh

##### Istio

### Security

#### OAuth

##### Identity Provider

###### Token Issuer

##### Authorization

###### Fine-grained control

