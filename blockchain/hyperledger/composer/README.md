# STANDALONE COMPOSER INSTRUCTIONS #
If wanting to run with the fabric project, read the fabric/README.md.

This Hyperledger Composer Business Network Application is fully runnable and can be executed with the composer-client project that is sibling to this. The Hyperledger Fabric BYFN tutorial is sufficient to locally deploy and test.

The model contains Individual and Organiation Participants, Assets for Services, Invoices and Goods and Transactions and Events tied to all.

COMMANDS
Follow the below commands to install and execute the project. Replace the idenities and card values with your own.

Inside composer project directory build the composer-bna@0.0.1.bna file:

% composer archive create -t dir -n . 

Install the business network:

% composer network install --card PeerAdmin@hlfv1 --archiveFile composer-bna@0.0.1.bna

Start the business network:

%  composer network start --networkName composer-bna --networkVersion 0.0.1 --networkAdmin admin --networkAdminEnrollSecret adminpw --card PeerAdmin@hlfv1 --file networkadmin.card

Import the business network card:

% composer card import --file networkadmin.card

Check that the deployment was successful:

% composer network ping -c admin@composer-bna

View the deployed network in composer playground:

% composer-playground

Open a browser and point to http://host:8080/login. The business network will appear under My Business Networks.

Create the REST API:

% composer-rest-server

Open a browser and point to http://host:3000/explorer.

Create Organisation participants either using the playground or REST API.

The Department organisation.
```
{
  "$class": "org.bcha.Organisation",
  "dateOfRegistration": "2018-10-10T00:32:31.464Z",
  "partyId": "1001",
  "email": "admin@department.gov",
  "name": "Department",
  "contactNo": "123456789",
  "active": true,
  "suspended": true,
  "address": {
    "$class": "org.bcha.Address",
    "addressLine1": "1 Department Drive"
  },
  "account": "resource:org.bcha.dCash#2001"
}
```

The Supplier orgsanisation.
```
{
  "$class": "org.bcha.Organisation",
  "dateOfRegistration": "2018-10-10T00:32:31.464Z",
  "partyId": "1002",
  "email": "admin@supplier.com",
  "name": "Supplier",
  "contactNo": "123456789",
  "active": true,
  "suspended": true,
  "address": {
    "$class": "org.bcha.Address",
    "addressLine1": "1/1 Supplier Circuit"
  },
  "account": "resource:org.bcha.dCash#2002"
}
```

Create the organisation dCash accounts with reference id's above.

The Department's account.
```
{
  "$class": "org.bcha.dCash",
  "accountId": "2001",
  "amount": 100,
  "owner": "resource:org.bcha.Organisation#1001"
}
```

The Supplier's account.
```
{
  "$class": "org.bcha.dCash",
  "accountId": "2002",
  "amount": 50,
  "owner": "resource:org.bcha.Organisation#1002"
}
```

Create a Good for computers that the Supplier owns.
```
{
  "$class": "org.bcha.Good",
  "quantity": 10,
  "tradeableId": "4001",
  "name": "Laptop",
  "description": "Laptop computer",
  "owner": "resource:org.bcha.Organisation#1002"
}
```

Create an Invoice for the Supplier for the Department to pay. The Supplier as the creator is the owner.
```
{
  "$class": "org.bcha.Invoice",
  "invoiceId": "3001",
  "document": "",
  "dueDate": "2018-10-10T00:48:35.176Z",
  "amount": 50,
  "tradeables": ["resource:org.bcha.Good#4001"],
  "payee": "resource:org.bcha.Organisation#1002",
  "payer": "resource:org.bcha.Organisation#1001",
  "owner": "resource:org.bcha.Organisation#1002"
}
```

Submit a sendInvoice transaction that will transfer the ownership from Supplier to Organisation.
```
{
  "$class": "org.bcha.SendInvoice",
  "invoice": "resource:org.bcha.Invoice#3001"
}
```

The owner of the Invoice#3001 will change. Looking at the Historian record (in the Playground) click on the Events(1) tab. An event similar to below will be there:
```
{
 "$class": "org.bcha.SendInvoiceEvent",
 "invoice": "resource:org.bcha.Invoice#3001",
 "emittedBy": "resource:org.hyperledger.composer.system.NetworkAdmin#admin",
 "eventId": "be3254c3040f5c8c4ad24db29865af5fdc11ab4e71c25a6a4baf5d4fdea05f39#0",
 "timestamp": "2018-10-10T00:51:48.853Z"
}
```

The owner field of the invoice record will change to: "owner": "resource:org.bcha.Organisation#1002"

At some stage the Department has to pay the invoice. It submits a payInvoice transaction that will transfer 50 dCash from the Department to the Supplier.
```
{
  "$class": "org.bcha.PayInvoice",
  "invoice": "resource:org.bcha.Invoice#3001"
}
```
	
Typically an invoice would not be paid until the goods and services it was for had been delivered or performed. Send a GoodTraded transaction as below:
```
{
  "$class": "org.bcha.GoodsTraded",
  "goods": ["resource:org.bcha.Good#4001"],
  "partyFrom": "resource:org.bcha.Organisation#1002",
  "partyTo": "resource:org.bcha.Organisation#1001"
}
```

Inspecting the Goods asset will show that its owner has changed. Like the invoice transactions, an event will have been fired for client applications (see the composer-client application) to receive and action against another software system such as an CRM or inventory management platform.