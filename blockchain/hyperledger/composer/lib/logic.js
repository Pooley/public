/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict'

/* global getAssetRegistry getParticipantRegistry getFactory getCurrentParticipant */

/**
 * @param {org.bcha.BasicEvent} basicEvent
 */
async function sendEvent(basicEvent) {
  basicEvent.emittedBy = getCurrentParticipant();
  
  emit(basicEvent);
}

/**
 * @param {org.bcha.SendInvoice} sendInvoice
 * @transaction
 */
async function onSendInvoice(sendInvoice) {
  let invoiceRegistry = await getAssetRegistry('org.bcha.Invoice');
  
  sendInvoice.invoice.owner = sendInvoice.invoice.payer;
  invoiceRegistry.update(sendInvoice.invoice);
  
  let sendInvoiceEvent = getFactory().newEvent('org.bcha', 'SendInvoiceEvent');
  sendInvoiceEvent.invoice = sendInvoice.invoice;
  sendEvent(sendInvoiceEvent);
}

/**
 * @param {org.bcha.PayInvoice} payInvoice
 * @transaction
 */
async function onPayInvoice(payInvoice) {
  let accountRegistry = await getAssetRegistry('org.bcha.dCash');
  
  let amount = payInvoice.invoice.amount;
  
  let payeeAccount = payInvoice.invoice.payee.account;
  let payerAccount = payInvoice.invoice.payer.account;
  
  let newPayerAccountAmount = payerAccount.amount - amount;
  if (newPayerAccountAmount < 0) {
    throw new Error('Payer does not have sufficient funds to pay the invoice');
  }
  payerAccount.amount = newPayerAccountAmount;
  accountRegistry.update(payerAccount);
  
  let newPayeeAccountAmount = payeeAccount.amount + amount;
  payeeAccount.amount = newPayeeAccountAmount;
  accountRegistry.update(payeeAccount);
  
  let payInvoiceEvent = getFactory().newEvent('org.bcha', 'PayInvoiceEvent');
  payInvoiceEvent.invoice = payInvoice.invoice;
  sendEvent(payInvoiceEvent);
}

/**
 * @param {org.bcha.GoodsTraded} goodsTraded
 * @transaction
 */
async function onGoodsTraded(goodsTraded) {
	let goodsRegistry = await getAssetRegistry('org.bcha.Good');  	

  	for (let n = 0; n < goodsTraded.goods.length; n++) {
      let good = goodsTraded.goods[n];
      
      if (good.owner !== goodsTraded.partyFrom) {
        throw new Error('Good is not owned by transiting Party');
      }
      
      good.owner = goodsTraded.partyTo;
      
      await goodsRegistry.update(good);
  	}
	
	let goodsTradedEvent = getFactory().newEvent('org.bcha', 'GoodsTradedEvent');
	goodsTradedEvent.goods = goodsTraded.goods;
	goodsTradedEvent.partyFrom = goodsTraded.partyFrom;
	goodsTradedEvent.partyTo = goodsTraded.partyTo;
	sendEvent(goodsTradedEvent);
}

/**
 * @param {org.bcha.ServicesProvided} servicesProvided
 * @transaction
 */
async function onServicesProvided(servicesProvided) {
	let serviceRegistry = await getAssetRegistry('org.bcha.Service');  	

  	for (let n = 0; n < servicesProvided.services.length; n++) {
      let service = servicesProvided.services[i];
      
      if (service.owner !== servicesProvided.partyFrom) {
        throw new Error('Service is not owned by providing Party');
      }
      
      service.owner = servicesProvided.partyTo;
      
      await serviceRegistry.update(service);
  	}
	
	let servicesProvidedEvent = getFactory().newEvent('org.bcha', 'ServicesProvidedEvent');
	servicesProvidedEvent.goods = servicesProvided.services;
	servicesProvidedEvent.partyFrom = servicesProvided.partyFrom;
	servicesProvidedEvent.partyTo = servicesProvided.partyTo;
	sendEvent(servicesProvidedEvent);
}
