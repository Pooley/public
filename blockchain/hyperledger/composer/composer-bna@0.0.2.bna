PK
     �iM���       package.json{"engines":{"composer":"^0.20.1"},"name":"composer-bna","version":"0.0.2","description":"Fully runnable composer project","scripts":{"prepublish":"mkdirp ./dist && composer archive create --sourceType dir --sourceName . -a ./dist/composer-bna.bna","pretest":"npm run lint","lint":"eslint .","test":"nyc mocha -t 0 test/*.js && cucumber-js"},"keywords":["composer","composer-network"],"author":"damian","email":"dapooley@gmail.com","license":"1","devDependencies":{"composer-admin":"^0.20.1","composer-cli":"^0.20.1","composer-client":"^0.20.1","composer-common":"^0.20.1","composer-connector-embedded":"^0.20.1","composer-cucumber-steps":"^0.20.1","chai":"latest","chai-as-promised":"latest","cucumber":"^2.2.0","eslint":"latest","nyc":"latest","mkdirp":"latest","mocha":"latest"}}PK
     �iM�3�  �  	   README.mdThis Hyperledger Composer Business Network Application is fully runnable and can be executed with the composer-client project that is sibling to this. The Hyperledger Fabric BYFN tutorial is sufficient to locally deploy and test.

The model contains Individual and Organiation Participants, Assets for Services, Invoices and Goods and Transactions and Events tied to all.

COMMANDS
Follow the below commands to install and execute the project. Replace the idenities and card values with your own.

Inside composer project directory build the composer-bna@0.0.1.bna file:

% composer archive create -t dir -n . 

Install the business network:

% composer network install --card PeerAdmin@hlfv1 --archiveFile composer-bna@0.0.1.bna

Start the business network:

%  composer network start --networkName composer-bna --networkVersion 0.0.1 --networkAdmin admin --networkAdminEnrollSecret adminpw --card PeerAdmin@hlfv1 --file networkadmin.card

Import the business network card:

% composer card import --file networkadmin.card

Check that the deployment was successful:

% composer network ping -c admin@composer-bna

View the deployed network in composer playground:

% composer-playground

Open a browser and point to http://host:8080/login. The business network will appear under My Business Networks.

Create the REST API:

% composer-rest-server

Open a browser and point to http://host:3000/explorer.

Create Organisation participants either using the playground or REST API.

The Department organisation.
```
{
  "$class": "org.bcha.Organisation",
  "dateOfRegistration": "2018-10-10T00:32:31.464Z",
  "partyId": "1001",
  "email": "admin@department.gov",
  "name": "Department",
  "contactNo": "123456789",
  "active": true,
  "suspended": true,
  "address": {
    "$class": "org.bcha.Address",
    "addressLine1": "1 Department Drive"
  },
  "account": "resource:org.bcha.dCash#2001"
}
```

The Supplier orgsanisation.
```
{
  "$class": "org.bcha.Organisation",
  "dateOfRegistration": "2018-10-10T00:32:31.464Z",
  "partyId": "1002",
  "email": "admin@supplier.com",
  "name": "Supplier",
  "contactNo": "123456789",
  "active": true,
  "suspended": true,
  "address": {
    "$class": "org.bcha.Address",
    "addressLine1": "1/1 Supplier Circuit"
  },
  "account": "resource:org.bcha.dCash#2002"
}
```

Create the organisation dCash accounts with reference id's above.

The Department's account.
```
{
  "$class": "org.bcha.dCash",
  "accountId": "2001",
  "amount": 100,
  "owner": "resource:org.bcha.Organisation#1001"
}
```

The Supplier's account.
```
{
  "$class": "org.bcha.dCash",
  "accountId": "2002",
  "amount": 50,
  "owner": "resource:org.bcha.Organisation#1002"
}
```

Create a Good for computers that the Supplier owns.
```
{
  "$class": "org.bcha.Good",
  "quantity": 10,
  "tradeableId": "4001",
  "name": "Laptop",
  "description": "Laptop computer",
  "owner": "resource:org.bcha.Organisation#1002"
}
```

Create an Invoice for the Supplier for the Department to pay. The Supplier as the creator is the owner.
```
{
  "$class": "org.bcha.Invoice",
  "invoiceId": "3001",
  "document": "",
  "dueDate": "2018-10-10T00:48:35.176Z",
  "amount": 50,
  "tradeables": ["resource:org.bcha.Good#4001"],
  "payee": "resource:org.bcha.Organisation#1002",
  "payer": "resource:org.bcha.Organisation#1001",
  "owner": "resource:org.bcha.Organisation#1002"
}
```

Submit a sendInvoice transaction that will transfer the ownership from Supplier to Organisation.
```
{
  "$class": "org.bcha.SendInvoice",
  "invoice": "resource:org.bcha.Invoice#3001"
}
```

The owner of the Invoice#3001 will change. Looking at the Historian record (in the Playground) click on the Events(1) tab. An event similar to below will be there:
```
{
 "$class": "org.bcha.SendInvoiceEvent",
 "invoice": "resource:org.bcha.Invoice#3001",
 "emittedBy": "resource:org.hyperledger.composer.system.NetworkAdmin#admin",
 "eventId": "be3254c3040f5c8c4ad24db29865af5fdc11ab4e71c25a6a4baf5d4fdea05f39#0",
 "timestamp": "2018-10-10T00:51:48.853Z"
}
```

The owner field of the invoice record will change to: "owner": "resource:org.bcha.Organisation#1002"

At some stage the Department has to pay the invoice. It submits a payInvoice transaction that will transfer 50 dCash from the Department to the Supplier.
```
{
  "$class": "org.bcha.PayInvoice",
  "invoice": "resource:org.bcha.Invoice#3001"
}
```
	
Typically an invoice would not be paid until the goods and services it was for had been delivered or performed. Send a GoodTraded transaction as below:
```
{
  "$class": "org.bcha.GoodsTraded",
  "goods": ["resource:org.bcha.Good#4001"],
  "partyFrom": "resource:org.bcha.Organisation#1002",
  "partyTo": "resource:org.bcha.Organisation#1001"
}
```

Inspecting the Goods asset will show that its owner has changed. Like the invoice transactions, an event will have been fired for client applications (see the composer-client application) to receive and action against another software system such as an CRM or inventory management platform.PK
     �iM%�F�  �     permissions.acl/**
 * Access control rules for composer-bna
 */
rule Default {
    description: "Allow all participants access to all resources"
    participant: "ANY"
    operation: ALL
    resource: "org.bcha.*"
    action: ALLOW
}

rule SystemACL {
  description:  "System ACL to permit all access"
  participant: "ANY"
  operation: ALL
  resource: "org.hyperledger.composer.system.**"
  action: ALLOW
}
PK
     �iM�9.7,   ,      queries.qry/** 
 * Queries for the business network
 */PK
     �iM               models/PK
     �iM����  �     models/org.bcha.cto/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace org.bcha

abstract participant Party identified by partyId {
  o String partyId
  o String email
  o String name
  o String contactNo
  o Boolean active
  o Boolean suspended
  o Address address
  --> dCash account
}

participant Organisation extends Party {
  o DateTime dateOfRegistration
  o DateTime dateOfDeregistration optional
  --> Organisation parentOrganisation optional
}

participant Person extends Party {
  o DateTime dateOfBirth
  o DateTime dateOfDeath optional
}

concept Address {
  o String addressLine1
  o String addressLine2 optional  
  o String city optional
  o String state optional
  o String postCode optional
  o String country optional
}

asset dCash identified by accountId {
  o String accountId
  o Double amount
  --> Party owner // the current Party who has the дCash
}

asset Invoice identified by invoiceId {
  o String invoiceId
  o String document // the compressed binary of the actual invoice
  o DateTime dueDate // when the amount must be paid to the payee by the payer
  o Double amount // amount of дCash the the payer pays the payee
  --> Tradeable[] tradeables // the goods and services the invoice is for
  --> Party payee // the party who created the invoice
  --> Party payer // the party who pays the invoice
  --> Party owner // the current Party who has the invoice
}

abstract asset Tradeable identified by tradeableId {
  o String tradeableId
  o String name
  o String description
  --> Party owner // the current Party who has the Tradeable
}

asset Good extends Tradeable {
  o Long quantity
}

asset Service extends Tradeable {
  o DateTime fromDate
  o DateTime toDate
  --> Party performedBy // the Party who performed the service
}

/**
 * Send and invoice to the Payee.
 */
transaction SendInvoice {
  --> Invoice invoice
}

/**
 * Payer pays the invoice amount to the payee.
 */
transaction PayInvoice {
  --> Invoice invoice
}

/*
 * Abstract movement of a tradeable between two Parties.
 */
abstract transaction tradeableTraded {
  --> Party partyFrom // the previous owner
  --> Party partyTo // the new owner
}

transaction GoodsTraded extends tradeableTraded {
  --> Good[] goods
}

transaction ServicesProvided extends tradeableTraded {
  --> Service[] services
}

abstract event BasicEvent {
  --> Participant emittedBy
}

abstract event TradeableTradedEvent extends BasicEvent {
  --> Party partyFrom // the previous owner
  --> Party partyTo // the new owner
}

event GoodsTradedEvent extends TradeableTradedEvent {
  --> Good[] goods
}

event ServicesProvidedEvent extends TradeableTradedEvent {
  --> Service[] services
}

event PayInvoiceEvent extends BasicEvent {
  --> Invoice invoice
}

event SendInvoiceEvent extends BasicEvent {
  --> Invoice invoice
}
PK
     �iM               lib/PK
     �iM�[J��  �     lib/logic.js/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict'

/* global getAssetRegistry getParticipantRegistry getFactory getCurrentParticipant */

/**
 * @param {org.bcha.BasicEvent} basicEvent
 */
async function sendEvent(basicEvent) {
  basicEvent.emittedBy = getCurrentParticipant();
  
  emit(basicEvent);
}

/**
 * @param {org.bcha.SendInvoice} sendInvoice
 * @transaction
 */
async function onSendInvoice(sendInvoice) {
  let invoiceRegistry = await getAssetRegistry('org.bcha.Invoice');
  
  sendInvoice.invoice.owner = sendInvoice.invoice.payer;
  invoiceRegistry.update(sendInvoice.invoice);
  
  let sendInvoiceEvent = getFactory().newEvent('org.bcha', 'SendInvoiceEvent');
  sendInvoiceEvent.invoice = sendInvoice.invoice;
  sendEvent(sendInvoiceEvent);
}

/**
 * @param {org.bcha.PayInvoice} payInvoice
 * @transaction
 */
async function onPayInvoice(payInvoice) {
  let accountRegistry = await getAssetRegistry('org.bcha.dCash');
  
  let amount = payInvoice.invoice.amount;
  
  let payeeAccount = payInvoice.invoice.payee.account;
  let payerAccount = payInvoice.invoice.payer.account;
  
  let newPayerAccountAmount = payerAccount.amount - amount;
  if (newPayerAccountAmount < 0) {
    throw new Error('Payer does not have sufficient funds to pay the invoice');
  }
  payerAccount.amount = newPayerAccountAmount;
  accountRegistry.update(payerAccount);
  
  let newPayeeAccountAmount = payeeAccount.amount + amount;
  payeeAccount.amount = newPayeeAccountAmount;
  accountRegistry.update(payeeAccount);
  
  let payInvoiceEvent = getFactory().newEvent('org.bcha', 'PayInvoiceEvent');
  payInvoiceEvent.invoice = payInvoice.invoice;
  sendEvent(payInvoiceEvent);
}

/**
 * @param {org.bcha.GoodsTraded} goodsTraded
 * @transaction
 */
async function onGoodsTraded(goodsTraded) {
	let goodsRegistry = await getAssetRegistry('org.bcha.Good');  	

  	for (let n = 0; n < goodsTraded.goods.length; n++) {
      let good = goodsTraded.goods[n];
      
      if (good.owner !== goodsTraded.partyFrom) {
        throw new Error('Good is not owned by transiting Party');
      }
      
      good.owner = goodsTraded.partyTo;
      
      await goodsRegistry.update(good);
  	}
	
	let goodsTradedEvent = getFactory().newEvent('org.bcha', 'GoodsTradedEvent');
	goodsTradedEvent.goods = goodsTraded.goods;
	goodsTradedEvent.partyFrom = goodsTraded.partyFrom;
	goodsTradedEvent.partyTo = goodsTraded.partyTo;
	sendEvent(goodsTradedEvent);
}

/**
 * @param {org.bcha.ServicesProvided} servicesProvided
 * @transaction
 */
async function onServicesProvided(servicesProvided) {
	let serviceRegistry = await getAssetRegistry('org.bcha.Service');  	

  	for (let n = 0; n < servicesProvided.services.length; n++) {
      let service = servicesProvided.services[i];
      
      if (service.owner !== servicesProvided.partyFrom) {
        throw new Error('Service is not owned by providing Party');
      }
      
      service.owner = servicesProvided.partyTo;
      
      await serviceRegistry.update(service);
  	}
	
	let servicesProvidedEvent = getFactory().newEvent('org.bcha', 'ServicesProvidedEvent');
	servicesProvidedEvent.goods = servicesProvided.services;
	servicesProvidedEvent.partyFrom = servicesProvided.partyFrom;
	servicesProvidedEvent.partyTo = servicesProvided.partyTo;
	sendEvent(servicesProvidedEvent);
}
PK 
     �iM���                     package.jsonPK 
     �iM�3�  �  	             7  README.mdPK 
     �iM%�F�  �               �  permissions.aclPK 
     �iM�9.7,   ,                �  queries.qryPK 
     �iM                          models/PK 
     �iM����  �               -  models/org.bcha.ctoPK 
     �iM                        G&  lib/PK 
     �iM�[J��  �               i&  lib/logic.jsPK      �  �5    