# BCHA #

The Blockchain Holding Authority (BCHA) is the organisation that chains the governance board and overseers/conducts most of the technical operations of the blockchain network for all the participants (such as reviewing Smart Contracts and ACL requests, onboarding new partners, checking the health, maintaining the primary orderer nodes, and providing disaster recovery).

# Fabric CA #

The Fabric CA Server was used to generate the msp artefacts for each of the nodes and users.

## Creating the affiliation structure ##
```
export CA_BIN=$HOME/fabric-ca/bin
export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/clients/admin
$CA_BIN/fabric-ca-client enroll -u http://admin:adminpw@localhost:7054
$CA_BIN/fabric-ca-client affiliation add bcha
$CA_BIN/fabric-ca-client affiliation add bcha.orderer
$CA_BIN/fabric-ca-client affiliation add bcha.peers
```

## Creating the orderer msp ##
```
export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/clients/admin
$CA_BIN/fabric-ca-client register --id.name orderer --id.secret ordererpw --id.type orderer --id.affiliation bcha.orderer
export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/orderer
$CA_BIN/fabric-ca-client enroll -u http://orderer:ordererpw@localhost:7054
```
The commands will create and populate folders:
```
Stored client certificate at ~/fabric-ca-platform/orderer/msp/signcerts/cert.pem
Stored root CA certificate at ~/fabric-ca-platform/orderer/msp/cacerts/localhost-7054.pem
Stored Issuer public key at ~/fabric-ca-platform/orderer/msp/IssuerPublicKey
Stored Issuer revocation public key at ~/fabric-ca-platform/orderer/msp/IssuerRevocationPublicKey
```

## Creating the peer msp ##
```
export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/clients/admin
$CA_BIN/fabric-ca-client register --id.name peer --id.secret peerpw --id.type peer --id.affiliation bcha.peers
export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/peers/peer
$CA_BIN/fabric-ca-client enroll -u http://peer:peerpw@localhost:7054
```
The commands will create and populate folders:
```
Stored client certificate at ~/fabric-ca-platform/peers/peer/msp/signcerts/cert.pem
Stored root CA certificate at ~/fabric-ca-platform/peers/peer/msp/cacerts/localhost-7054.pem
Stored Issuer public key at ~/fabric-ca-platform/peers/peer/msp/IssuerPublicKey
Stored Issuer revocation public key at ~/fabric-ca-platform/peers/peer/msp/IssuerRevocationPublicKey
```

## Creating the bcha administration user msp ##
```
export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/clients/admin
$CA_BIN/fabric-ca-client register --id.name Admin --id.secret adminpw --id.type user --id.affiliation bcha
export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/users/Admin
$CA_BIN/fabric-ca-client enroll -u http://Admin:adminpw@localhost:7054

export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/client/certs
$CA_BIN/fabric-ca-client getcacert -u http://admin:adminpw@localhost:7054
```

## Structuring the files for network generation ##
The CA does not produce the files with the naming or folder convention that the configtxgen tool requires from the configuration in the configtx.yaml and docker-compose.yml files. The next set of commands move and rename files into the require folder structure.

The crypto-config tree structure for bcha will be:
```
crypto-config
    + ordererOrganizations
	    + bcha.com
			+ ca
			+ msp
			    + admincerts
				+ cacerts
			+ orderers
			    + orderer.bcha.com
				    + msp
					    + admincerts
						+ cacerts
						+ keystore
						+ signcerts
		    + users
			    + Admin@bcha.com
				    + msp
					    + admincerts
						+ cacerts
						+ keystore
						+ signcerts
    + peerOrganizations
	    + bcha.com
			+ ca
			+ msp 
			    + admincerts
				+ cacerts
			+ peers
			    + peer.bcha.com
				    + msp
						+ admincerts
						+ cacerts
						+ keystore
						+ signcerts
			+ users
			    + Admin@bcha.com
				    + msp
					    + admincerts
						+ cacerts
						+ keystore
						+ signcerts
```

The commands to create and populate the ordererOrganizations:
```
mkdir -p ~/crypto-config/ordererOrganizations/bcha.com
mkdir -p ~/crypto-config/ordererOrganizations/bcha.com/orderers/orderer.bcha.com
cp -a ~/fabric-ca-platform/orderer/msp ~/crypto-config/ordererOrganizations/bcha.com/orderers/orderer.bcha.com/
mv ~/crypto-config/ordererOrganizations/bcha.com/orderers/orderer.bcha.com/msp/cacerts/localhost-7054.pem ~/crypto-config/ordererOrganizations/bcha.com/orderers/orderer.bcha.com/msp/cacerts/ca.bcha.com-cert.pem
mv ~/crypto-config/ordererOrganizations/bcha.com/orderers/orderer.bcha.com/msp/signcerts/cert.pem ~/crypto-config/ordererOrganizations/bcha.com/orderers/orderer.bcha.com/msp/signcerts/orderer.bcha.com-cert.pem
mkdir -p ~/crypto-config/ordererOrganizations/bcha.com/orderers/orderer.bcha.com/msp/admincerts
cp ~/fabric-ca-platform/users/Admin/msp/signcerts/cert.pem ~/crypto-config/ordererOrganizations/bcha.com/orderers/orderer.bcha.com/msp/admincerts/Admin@bcha.com-cert.pem

cp -a ~/fabric-ca-platform/users ~/crypto-config/ordererOrganizations/bcha.com/
mv ~/crypto-config/ordererOrganizations/bcha.com/users/Admin/msp/cacerts/localhost-7054.pem ~/crypto-config/ordererOrganizations/bcha.com/users/Admin/msp/cacerts/ca.bcha.com-cert.pem
mv ~/crypto-config/ordererOrganizations/bcha.com/users/Admin/msp/signcerts/cert.pem ~/crypto-config/ordererOrganizations/bcha.com/users/Admin/msp/signcerts/Admin@bcha.com-cert.pem
mkdir -p ~/crypto-config/ordererOrganizations/bcha.com/users/Admin/msp/admincerts
cp ~/crypto-config/ordererOrganizations/bcha.com/users/Admin/msp/signcerts/Admin@bcha.com-cert.pem ~/crypto-config/ordererOrganizations/bcha.com/users/Admin/msp/admincerts/Admin@bcha.com-cert.pem

cp -a ~/fabric-ca-platform/client/certs/msp ~/crypto-config/ordererOrganizations/bcha.com/
mkdir -p ~/crypto-config/ordererOrganizations/bcha.com/msp/admincerts
cp ~/crypto-config/ordererOrganizations/bcha.com/users/Admin/msp/admincerts/Admin@bcha.com-cert.pem ~/crypto-config/ordererOrganizations/bcha.com/msp/admincerts
mv ~/crypto-config/ordererOrganizations/bcha.com/msp/cacerts/localhost-7054.pem ~/crypto-config/ordererOrganizations/bcha.com/msp/cacerts/ca.bcha.com-cert.pem

mv ~/crypto-config/ordererOrganizations/bcha.com/users/Admin ~/crypto-config/ordererOrganizations/bcha.com/users/Admin@bcha.com
```

The commands to create and populate the peers:
```
mkdir -p ~/crypto-config/peerOrganizations/bcha.com
cp -a ~/crypto-config/ordererOrganizations/bcha.com/msp ~/crypto-config/peerOrganizations/bcha.com
cp -a ~/crypto-config/ordererOrganizations/bcha.com/users/ ~/crypto-config/peerOrganizations/bcha.com

mkdir -p ~/crypto-config/peerOrganizations/bcha.com/peers/peer.bcha.com
cp -a ~/fabric-ca-platform/peers/peer/msp ~/crypto-config/peerOrganizations/bcha.com/peers/peer.bcha.com/
mv ~/crypto-config/peerOrganizations/bcha.com/peers/peer.bcha.com/msp/cacerts/localhost-7054.pem ~/crypto-config/peerOrganizations/bcha.com/peers/peer.bcha.com/msp/cacerts/ca.bcha.com-cert.pem
mv ~/crypto-config/peerOrganizations/bcha.com/peers/peer.bcha.com/msp/signcerts/cert.pem ~/crypto-config/peerOrganizations/bcha.com/peers/peer.bcha.com/msp/signcerts/peer.bcha.com-cert.pem
mkdir -p ~/crypto-config/peerOrganizations/bcha.com/peers/peer.bcha.com/msp/admincerts
cp ~/fabric-ca-platform/users/Admin/msp/signcerts/cert.pem ~/crypto-config/peerOrganizations/bcha.com/peers/peer.bcha.com/msp/admincerts/Admin@bcha.com-cert.pem
```

Finally to populate the ca directories for the orderers and peers.
```
mkdir -p ~/crypto-config/ordererOrganizations/bcha.com/ca/
cp ~/fabric-ca/bin/msp/keystore/*sk ~/crypto-config/ordererOrganizations/bcha.com/ca/
cp ~/fabric-ca/bin/ca-cert.pem ~/crypto-config/ordererOrganizations/bcha.com/ca/ca.bcha.com-cert.pem

cp -a ~/crypto-config/peerOrganizations/bcha.com/ca/ ~/crypto-config/peerOrganizations/bcha.com
```

## Troubleshooting ##
https://blog.knoldus.com/hyperledger-fabric-certificate-authority/

https://gerrit.hyperledger.org/r/#/c/10871/6/examples/e2e_cli/fabric-ca-cryptogen.sh