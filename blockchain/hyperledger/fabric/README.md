# BCHA Network # 

The fabric project creates a Hyperledger Fabric network with three participating entities.

BCHA: The main administration party who is sets up the network and chairs the governance body. The BCHA operates the orderer node and peers.

Department: A large entity that deals with many other entities. The Department operates peers.

Supplier: An entity that primarily deals with the Department. The Supplier operates peers.

## Setup ##

Go through the entities in order. The BCHA sets up the network and the chain's genesis blocks. The Department and Supplier are added to the network and do not have the seed files.

### BCHA ###

Create the system channel genesis block
```
export BIN=/home/ubuntu/fabric-samples/bin
export FABRIC_CFG_PATH=~/public/blockchain/hyperledger/fabric/bcha
$BIN/configtxgen -profile bchaOrdererGenesis -channelID bcha-sys-channel -outputBlock /home/ubuntu/public/blockchain/hyperledger/fabric/bcha/channel-artifacts/genesis.block
```

Create the channel configuration transaction
```
$BIN/configtxgen -profile bchachannel -outputCreateChannelTx /home/ubuntu/public/blockchain/hyperledger/fabric/bcha/channel-artifacts/channel.tx -channelID bchachannel
```

Set the bcha's Anchor Peer (it only has 1 peer).
```
$BIN/configtxgen -profile bchachannel -outputAnchorPeersUpdate /home/ubuntu/public/blockchain/hyperledger/fabric/bcha/channel-artifacts/bchaMSPanchors.tx -channelID bchachannel -asOrg bchaMSP
```

Start the docker images using the docker-compose.yml file.
```
docker-compose -f /home/ubuntu/public/blockchain/hyperledger/fabric/bcha/docker-compose.yml up -d
```
Terminal into the cli image (in a production setting only the cli machine should be reachable by ssh/tty).
```
docker exec -it cli bash
```
Create and join the channel.
```
peer channel create -o orderer.bcha.com:7050 -c bchachannel -f ./config/channel.tx
peer channel join -b bchachannel.block
peer channel update -o orderer.bcha.com:7050 -c bchachannel -f ./config/bchaMSPanchors.tx
peer channel list
```
The output of the last command indicates that the channel is created and the peer connected to it. Hyperledger Fabric has now been configured and is running.

Run the docker-compose commands to stop and start the containers. There is no need to join to the channel again after a restart.

#### Deploy the composer project onto the fabric network ####

To deploy the composer network onto the bcha fabric, we need a administrator role Business Network Card (BNC). A BNC is the artifact that allows a public/private pair of certificates to connect the Hyperledger Fabric containers. Return to the composer project.
```
composer card create -p /home/ubuntu/public/blockchain/hyperledger/composer/connection.json -u bchaAdmin -c /home/ubuntu/public/blockchain/hyperledger/fabric/bcha/crypto-config/peerOrganizations/bcha.com/users/Admin@bcha.com/msp/signcerts/Admin@bcha.com-cert.pem -k /home/ubuntu/public/blockchain/hyperledger/fabric/bcha/crypto-config/peerOrganizations/bcha.com/users/Admin@bcha.com/msp/keystore/1a340a0f388d5947e916260bd0624ec4e545f9d143b6eb2f2818fd3a3fde45eb_sk -r PeerAdmin -r ChannelAdmin
composer card import -f /home/ubuntu/public/blockchain/hyperledger/composer/bchaAdmin@composer-bcha.card
```

The output of the commands will be:
```
Successfully created business network card file to
        Output file: bchaAdmin@composer-bcha.card
		
Successfully imported business network card
        Card file: /home/ubuntu/public/blockchain/hyperledger/composer/bchaAdmin@composer-bcha.card
        Card name: bchaAdmin@composer-bcha
```

With the BNC created and imported the composer network project can be installed onto the Fabric. The project is turned into a deployable archive (business network archive), installed and lastly started.
```
composer archive create -t dir -n /home/ubuntu/public/blockchain/hyperledger/composer/
composer network install --card bchaAdmin@composer-bcha --archiveFile /home/ubuntu/public/blockchain/hyperledger/composer/composer-bna@0.0.1.bna
```

An administration user for the installed composet network needs to be created for bcha so they can start the network and perform other activities. The main administrator account, bchaAdmin, generates an identity in the Fabcric-CA bchaComposerAdmin and when starting the composer netowrk, binds bchaComposerAdmin to it as the composer network administartor. For bchaComposerAdmin to interact with the composer network, it needs a business network card (BNC or Card), and it has to be put into the composer Wallet.
```
composer identity request -c bchaAdmin@composer-bcha -u admin -s adminpw -d bchaComposerAdmin
composer network start -c bchaAdmin@composer-bcha -n composer-bna -V 0.0.1 -o endorsementPolicyFile=~/public/blockchain/hyperledger/composer/endorsement-policy.json -A bchaComposerAdmin -C /home/ubuntu/public/blockchain/hyperledger/composer/bchaComposerAdmin/admin-pub.pem 
composer card create -p /home/ubuntu/public/blockchain/hyperledger/composer/connection.json -u bchaComposerAdmin -n composer-bna -c /home/ubuntu/public/blockchain/hyperledger/composer/bchaComposerAdmin/admin-pub.pem -k /home/ubuntu/public/blockchain/hyperledger/composer/bchaComposerAdmin/admin-priv.pem

composer card import -f /home/ubuntu/public/blockchain/hyperledger/composer/bchaComposerAdmin@composer-bna.card
composer network list -c bchaComposerAdmin@composer-bna
composer network ping -c bchaComposerAdmin@composer-bna
composer identity list -c bchaComposerAdmin@composer-bna
```
The business network is now ready to use. Run the composer-playground and connect with bchaComposerAdmin, you can now view the contents of the composer business network archive that was just installed and started, and test out its transactions.

The users created and used so far have been the fabric and composer administrators. Real users of the blockchain's functionality are created by the steps below. Each user interacting with the composer project must be a Participant and have their own business network card bound to it.
```
composer participant add -c bchaComposerAdmin@composer-bna -d '{"$class": "com.bcha.Person","dateOfBirth": "2018-10-10T00:32:31.464Z","partyId": "001","email": "damian@bcha.com","name": "Damian","contactNo": "123456789","active": true,"suspended": true,"address": {"$class": "com.bcha.Address","addressLine1": "1 BCHA Street"},"account": "resource:com.bcha.dCash#001"}'
composer identity issue -c bchaComposerAdmin@composer-bna -f damian.card -u damian -a "resource:com.bcha.Person#001"
composer card import -f damian@composer-bna.card
composer network ping -c damian@composer-bna
composer transaction submit --card damian@composer-bna -d '{"$class": "org.hyperledger.composer.system.AddAsset", "targetRegistry" : "resource:org.hyperledger.composer.system.AssetRegistry#com.bcha.dCash", "resources": [{"$class": "com.bcha.dCash","accountId":"damian", "amount":"100","owner":"resource:com.bcha.Person#001"}]}'
composer network list -c damian@composer-bna
```

## Onboarding the Department ##
Organisations will be onboarded (and removed) from the blockchain network throughout its journey. The onboarding Organisations do not have to set up the blockchain from scratch, nor will or should they know the accounts, identities and passwords of the existing organisations on the blockchain. The following steps onboards the Department organisation and knowing on the details and configuration relevant to them.

The department only defines the configuration that it needs, the crypto-config, docker images etc.
```
export FABRIC_CFG_PATH=/home/ubuntu/public/blockchain/hyperledger/fabric/department
mkdir -p /home/ubuntu/public/blockchain/hyperledger/fabric/department/channel-artifacts
$BIN/configtxgen -printOrg department > /home/ubuntu/public/blockchain/hyperledger/fabric/department/channel-artifacts/department.json
```

The bcha orderer has to onboard the department into the hyperledger fabric. Through a secure process the department sends a copy of the above generated department.json to the bcha orderer administrators, who then update the configuration block with the department details.
```
cp ../../department/channel-artifacts/department.json

docker exec -it cli bash

peer channel fetch config config_block.pb -o orderer.bcha.com:7050 -c bchachannel
configtxlator proto_decode --input config_block.pb --type common.Block | jq .data.data[0].payload.data.config > config.json

jq -s '.[0] * {"channel_group":{"groups":{"Application":{"groups": {"departmentMSP":.[1]}}}}}' config.json ./config/department.json > modified_config.json
configtxlator proto_encode --input config.json --type common.Config --output config.pb
configtxlator proto_encode --input modified_config.json --type common.Config --output modified_config.pb
configtxlator compute_update --channel_id bchachannel --original config.pb --updated modified_config.pb --output department_update.pb
configtxlator proto_decode --input department_update.pb --type common.ConfigUpdate | jq . > department_update.json
echo '{"payload":{"header":{"channel_header":{"channel_id":"bchachannel", "type":2}},"data":{"config_update":'$(cat department_update.json)'}}}' | jq . > department_update_in_envelope.json
configtxlator proto_encode --input department_update_in_envelope.json --type common.Envelope --output department_update_in_envelope.pb

peer channel signconfigtx -f department_update_in_envelope.pb -- only bcha org to sign

peer channel update -f department_update_in_envelope.pb -c bchachannel -o orderer.bcha.com:7050
```

The department joins its fabric docker images to the channel and the rest of the fabric network.
```
docker-compose -f /home/ubuntu/public/blockchain/hyperledger/fabric/department/docker-compose.yml up -d
docker exec -it departmentCli bash
peer channel fetch 0 bchachannel.block -o orderer.bcha.com:7050 -c bchachannel
peer channel join -b bchachannel.block

export CORE_PEER_ADDRESS=peer1.department.com:7051
peer channel join -b bchachannel.block
```

The Department creates a card for its peer admin as well as a composer admin user. The Department has been given the original connection.json and altered it by adding their own organisation, peers and certificate authority (department_connection_department.json). The Department has also been given the composer network archive and installs it on their peers.
```
composer card create -p /home/ubuntu/public/blockchain/hyperledger/composer/department_connection_department.json -u departmentAdmin -c /home/ubuntu/public/blockchain/hyperledger/fabric/department/crypto-config/peerOrganizations/department.com/users/Admin@department.com/msp/signcerts/Admin@department.com-cert.pem -k /home/ubuntu/public/blockchain/hyperledger/fabric/department/crypto-config/peerOrganizations/department.com/users/Admin@department.com/msp/keystore/f921764463de090b5db3d6758d5ebfa55d7cf89c4d0467b35feee1065d24e062_sk -r PeerAdmin -r ChannelAdmin
composer card import -f /home/ubuntu/public/blockchain/hyperledger/composer/departmentAdmin@composer-bcha.card
composer network install --card departmentAdmin@composer-bcha --archiveFile /home/ubuntu/public/blockchain/hyperledger/composer/composer-bna@0.0.1.bna

composer identity request -c departmentAdmin@composer-bcha -u admin -s adminpw -d departmentComposerAdmin
composer participant add -c bchaComposerAdmin@composer-bna -d '{"$class":"org.hyperledger.composer.system.NetworkAdmin", "participantId":"departmentComposerAdmin"}'
composer identity bind   -c bchaComposerAdmin@composer-bna -a "resource:org.hyperledger.composer.system.NetworkAdmin#departmentComposerAdmin" -e /home/ubuntu/public/blockchain/hyperledger/composer/departmentComposerAdmin/admin-pub.pem

composer card create -p /home/ubuntu/public/blockchain/hyperledger/composer/department_connection_department.json -u departmentComposerAdmin -n composer-bna -c /home/ubuntu/public/blockchain/hyperledger/composer/departmentComposerAdmin/admin-pub.pem -k /home/ubuntu/public/blockchain/hyperledger/composer/departmentComposerAdmin/admin-priv.pem
composer card import -f /home/ubuntu/public/blockchain/hyperledger/composer/departmentComposerAdmin@composer-bna.card
composer network ping -c departmentComposerAdmin@composer-bna
composer identity list -c departmentComposerAdmin@composer-bna
composer network list -c departmentComposerAdmin@composer-bna

composer participant add -c departmentComposerAdmin@composer-bna -d '{"$class": "com.bcha.Person","dateOfBirth": "2018-10-10T00:32:31.464Z","partyId": "003","email": "deptartmentmanager@department.com","name": "deptartmentmanager","contactNo": "123456789","active": true,"suspended": true,"address": {"$class": "com.bcha.Address","addressLine1": "1 Department Drive"},"account": "resource:com.bcha.dCash#003"}'
composer identity issue -c departmentComposerAdmin@composer-bna -f deptartmentmanager.card -u deptartmentmanager -a "resource:com.bcha.Person#003"
composer card import -f deptartmentmanager.card
composer network ping -c deptartmentmanager@composer-bna
composer transaction submit --card deptartmentmanager@composer-bna -d '{"$class": "org.hyperledger.composer.system.AddAsset", "targetRegistry" : "resource:org.hyperledger.composer.system.AssetRegistry#com.bcha.dCash", "resources": [{"$class": "com.bcha.dCash","accountId":"003", "amount":"100","owner":"resource:com.bcha.Person#003"}]}'
composer network list -c deptartmentmanager@composer-bna

```

### BCHA updating its Business Network Cards ###
Unfortunately at this time existing business network cards are not automatically updated with the new peers. bchaComposerAdmin must replace its card and include the department peer information that has been shared to it by the Department blockchain administartors. This step will have to be repeated for every card across all members of the network.
```
composer card delete -c bchaComposerAdmin@composer-bna
composer card create -p /home/ubuntu/public/blockchain/hyperledger/composer/bcha_connection_department.json -u bchaComposerAdmin -n composer-bna -c /home/ubuntu/public/blockchain/hyperledger/composer/bchaComposerAdmin/admin-pub.pem -k /home/ubuntu/public/blockchain/hyperledger/composer/bchaComposerAdmin/admin-priv.pem
composer card import -f /home/ubuntu/public/blockchain/hyperledger/composer/bchaComposerAdmin@composer-bna.card
composer network ping -c bchaComposerAdmin@composer-bna
composer identity list -c bchaComposerAdmin@composer-bna
```

## Redeploying the composer network with Department endosers ##
Changing the endorsement policy of a composer network requires the same process as any other change: increasing the network's version, rearchiving, installing on all the peers and starting it again. The coordination effort is expotentially difficult the more peers and participating organisations there are involved.

```
vi package.json
-- change version, :wq
composer archive create -t dir -n .
composer network install --card bchaAdmin@composer-bcha --archiveFile /home/ubuntu/public/blockchain/hyperledger/composer/composer-bna@0.0.2.bna
composer network upgrade -n composer-bna -V 0.0.2 -o /home/ubuntu/public/blockchain/hyperledger/composer/endorsement-policy-department.json --card bchaAdmin@composer-bcha
```

Each organisation's composer administrator should verify that the version has changed:
```
docker exec -it cli bash
peer chaincode list --installed
	>Get installed chaincodes on peer:
	>Name: composer-bna, Version: 0.0.1, Path: /tmp/businessnetwork118109-30012-whny7o.oc6wr, Id: 6b173e41dd9c3287f6edaa9aa709e162b905ddf54626b83faf481fad18599397
	>Name: composer-bna, Version: 0.0.2, Path: /tmp/businessnetwork118109-1170-1kl7x26.1xen, Id: 50bc994aac2af2c6975f6db6dd9b09c1c2bbdc2d965d2e500b270eeb773f6e9a
peer chaincode list --instantiated -C bchachannel
	>Get instantiated chaincodes on channel bchachannel:
	>Name: composer-bna, Version: 0.0.2, Path: /tmp/businessnetwork118109-1170-1kl7x26.1xen, Input: <nil>, Escc: escc, Vscc: vscc
```
The Department also verifies with the same commands:
```
composer network install --card departmentAdmin@composer-bcha --archiveFile /home/ubuntu/public/blockchain/hyperledger/composer/composer-bna@0.0.2.bna
peer chaincode list --installed
peer chaincode list --instantiated -C bchachannel
```

## RESTARTING CLEANUP ##
composer card delete -c bchaComposerAdmin@composer-bna
composer card delete -c bchaAdmin@composer-bcha
composer card delete -c damian@composer-bcha

docker-compose -f docker-compose.yml down
docker volume prune