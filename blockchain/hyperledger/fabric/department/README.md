# The Department #

The Department is the organisation that is the primary customer in the blockchain. The Department has its own network to host the blockchain.

# Fabric CA #

The Fabric CA Server was used to generate the msp artefacts for each of the nodes and users.

## Creating the affiliation structure ##
```
export CA_BIN=$HOME/fabric-ca/bin
export DEPT_HOME=/home/ubuntu/public/blockchain/hyperledger/fabric/department/fabric-ca-platform
export FABRIC_CA_CLIENT_HOME=$DEPT_HOME/clients/admin
$CA_BIN/fabric-ca-client enroll -u http://admin:adminpw@localhost:7054
$CA_BIN/fabric-ca-client affiliation add department
$CA_BIN/fabric-ca-client affiliation add department.peers
```

## Creating the peer0 msp ##
```
export FABRIC_CA_CLIENT_HOME=$DEPT_HOME/clients/admin
$CA_BIN/fabric-ca-client register --id.name peer0 --id.secret peerpw --id.type peer --id.affiliation department.peers
export FABRIC_CA_CLIENT_HOME=$DEPT_HOME/peers/peer0
$CA_BIN/fabric-ca-client enroll -u http://peer0:peerpw@localhost:7054
```

The commands will create and populate folders:
```
Stored client certificate at /home/ubuntu/public/blockchain/hyperledger/fabric/department/fabric-ca-platform/peers/peer0/msp/signcerts/cert.pem
Stored root CA certificate at /home/ubuntu/public/blockchain/hyperledger/fabric/department/fabric-ca-platform/peers/peer0/msp/cacerts/localhost-7054.pem
Stored Issuer public key at /home/ubuntu/public/blockchain/hyperledger/fabric/department/fabric-ca-platform/peers/peer0/msp/IssuerPublicKey
Stored Issuer revocation public key at /home/ubuntu/public/blockchain/hyperledger/fabric/department/fabric-ca-platform/peers/peer0/msp/IssuerRevocationPublicKey
```

## Creating the peer1 msp ##
```
export FABRIC_CA_CLIENT_HOME=$DEPT_HOME/clients/admin
$CA_BIN/fabric-ca-client register --id.name peer1 --id.secret peerpw --id.type peer --id.affiliation department.peers
export FABRIC_CA_CLIENT_HOME=$DEPT_HOME/peers/peer1
$CA_BIN/fabric-ca-client enroll -u http://peer1:peerpw@localhost:7054
```

The commands will create and populate folders:
```
Stored client certificate at /home/ubuntu/public/blockchain/hyperledger/fabric/department/fabric-ca-platform/peers/peer1/msp/signcerts/cert.pem
Stored root CA certificate at /home/ubuntu/public/blockchain/hyperledger/fabric/department/fabric-ca-platform/peers/peer1/msp/cacerts/localhost-7054.pem
Stored Issuer public key at /home/ubuntu/public/blockchain/hyperledger/fabric/department/fabric-ca-platform/peers/peer1/msp/IssuerPublicKey
Stored Issuer revocation public key at /home/ubuntu/public/blockchain/hyperledger/fabric/department/fabric-ca-platform/peers1/peer/msp/IssuerRevocationPublicKey
```

## Creating the bcha administration user msp ##
```
export FABRIC_CA_CLIENT_HOME=$DEPT_HOME/clients/admin
$CA_BIN/fabric-ca-client register --id.name Admin --id.secret adminpw --id.type user --id.affiliation department
export FABRIC_CA_CLIENT_HOME=$DEPT_HOME/users/Admin
$CA_BIN/fabric-ca-client enroll -u http://Admin:adminpw@localhost:7054
```

## Extract the ca certificates ##
```
export FABRIC_CA_CLIENT_HOME=$DEPT_HOME/client/certs
$CA_BIN/fabric-ca-client getcacert -u http://admin:adminpw@localhost:7054
```

## Structuring the files for network generation ##
The CA does not produce the files with the naming or folder convention that the configtxgen tool requires from the configuration in the configtx.yaml and docker-compose.yml files. The next set of commands move and rename files into the require folder structure.

The crypto-config tree structure for the department will be:
```
crypto-config
    + peerOrganizations
	    + department.com
			+ ca
			+ msp 
			    + admincerts
				+ cacerts
			+ peers
			    + peer0.department.com
				    + msp
						+ admincerts
						+ cacerts
						+ keystore
						+ signcerts
				+ peer1.department.com
				    + msp
						+ admincerts
						+ cacerts
						+ keystore
						+ signcerts
			+ users
			    + Admin@department.com
				    + msp
					    + admincerts
						+ cacerts
						+ keystore
						+ signcerts
```

The commands to create and populate the peers:
```
export DEPT_CRYPTO=/home/ubuntu/public/blockchain/hyperledger/fabric/department/crypto-config
mkdir -p $DEPT_CRYPTO/peerOrganizations/department.com

mkdir -p $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer0.department.com
cp -a $DEPT_HOME/peers/peer0/msp $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer0.department.com/
mv $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer0.department.com/msp/cacerts/localhost-7054.pem $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer0.department.com/msp/cacerts/ca.department.com-cert.pem
mv $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer0.department.com/msp/signcerts/cert.pem $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer0.department.com/msp/signcerts/peer0.department.com-cert.pem
mkdir -p $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer0.department.com/msp/admincerts
cp $DEPT_HOME/users/Admin/msp/signcerts/cert.pem $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer0.department.com/msp/admincerts/Admin@department.com-cert.pem

mkdir -p $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer1.department.com
cp -a $DEPT_HOME/peers/peer1/msp $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer1.department.com/
mv $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer1.department.com/msp/cacerts/localhost-7054.pem $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer1.department.com/msp/cacerts/ca.department.com-cert.pem
mv $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer1.department.com/msp/signcerts/cert.pem $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer1.department.com/msp/signcerts/peer1.department.com-cert.pem
mkdir -p $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer1.department.com/msp/admincerts
cp $DEPT_HOME/users/Admin/msp/signcerts/cert.pem $DEPT_CRYPTO/peerOrganizations/department.com/peers/peer1.department.com/msp/admincerts/Admin@department.com-cert.pem

cp -a $DEPT_HOME/users $DEPT_CRYPTO/peerOrganizations/department.com/
mv $DEPT_CRYPTO/peerOrganizations/department.com/users/Admin/msp/cacerts/localhost-7054.pem $DEPT_CRYPTO/peerOrganizations/department.com/users/Admin/msp/cacerts/ca.department.com-cert.pem
mv $DEPT_CRYPTO/peerOrganizations/department.com/users/Admin/msp/signcerts/cert.pem $DEPT_CRYPTO/peerOrganizations/department.com/users/Admin/msp/signcerts/Admin@department.com-cert.pem
mkdir -p $DEPT_CRYPTO/peerOrganizations/department.com/users/Admin/msp/admincerts
cp $DEPT_CRYPTO/peerOrganizations/department.com/users/Admin/msp/signcerts/Admin@department.com-cert.pem $DEPT_CRYPTO/peerOrganizations/department.com/users/Admin/msp/admincerts/Admin@department.com-cert.pem

cp -a $DEPT_HOME/client/certs/msp $DEPT_CRYPTO/peerOrganizations/department.com/
mkdir -p $DEPT_CRYPTO/peerOrganizations/department.com/msp/admincerts
cp $DEPT_CRYPTO/peerOrganizations/department.com/users/Admin/msp/admincerts/Admin@department.com-cert.pem $DEPT_CRYPTO/peerOrganizations/department.com/msp/admincerts
mv $DEPT_CRYPTO/peerOrganizations/department.com/msp/cacerts/localhost-7054.pem $DEPT_CRYPTO/peerOrganizations/department.com/msp/cacerts/ca.department.com-cert.pem

mv $DEPT_CRYPTO/peerOrganizations/department.com/users/Admin $DEPT_CRYPTO/peerOrganizations/department.com/users/Admin@department.com

mkdir -p $DEPT_CRYPTO/peerOrganizations/department.com/ca/
cp $CA_BIN/msp/keystore/*sk $DEPT_CRYPTO/peerOrganizations/department.com/ca/
cp $CA_BIN/ca-cert.pem $DEPT_CRYPTO/peerOrganizations/department.com/ca/ca.department.com-cert.pem
```

## Troubleshooting ##
https://gerrit.hyperledger.org/r/#/c/10871/6/examples/e2e_cli/fabric-ca-cryptogen.sh