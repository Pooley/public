# The Supplier #

The Supplier is the organisation that primarily transacts only with the Department. The Supplier has its own network to host the blockchain.

# Fabric CA #

The Fabric CA Server was used to generate the msp artefacts for each of the nodes and users.

## Creating the affiliation structure ##
```
export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/clients/admin
./fabric-ca-client enroll -u http://admin:adminpw@localhost:7054
./fabric-ca-client affiliation add supplier
./fabric-ca-client affiliation add supplier.peers
```

## Creating the peer0 msp ##
```
export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/clients/admin
./fabric-ca-client register --id.name peer0 --id.secret peerpw --id.type peer --id.affiliation supplier.peers
export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/peers/peer0
./fabric-ca-client enroll -u http://peer0:peerpw@localhost:7054
```

The commands will create and populate folders:
```
Stored client certificate at /home/ubuntu/fabric-ca-platform/peers/peer0/msp/signcerts/cert.pem
Stored root CA certificate at /home/ubuntu/fabric-ca-platform/peers/peer0/msp/cacerts/localhost-7054.pem
Stored Issuer public key at /home/ubuntu/fabric-ca-platform/peers/peer0/msp/IssuerPublicKey
Stored Issuer revocation public key at /home/ubuntu/fabric-ca-platform/peers/peer0/msp/IssuerRevocationPublicKey
```

## Creating the peer1 msp ##
```
export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/clients/admin
./fabric-ca-client register --id.name peer1 --id.secret peerpw --id.type peer --id.affiliation supplier.peers
export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/peers/peer1
./fabric-ca-client enroll -u http://peer1:peerpw@localhost:7054
```

The commands will create and populate folders:
```
Stored client certificate at /home/ubuntu/fabric-ca-platform/peers/peer1/msp/signcerts/cert.pem
Stored root CA certificate at /home/ubuntu/fabric-ca-platform/peers/peer1/msp/cacerts/localhost-7054.pem
Stored Issuer public key at /home/ubuntu/fabric-ca-platform/peers/peer1/msp/IssuerPublicKey
Stored Issuer revocation public key at /home/ubuntu/fabric-ca-platform/peers1/peer/msp/IssuerRevocationPublicKey
```

## Creating the bcha administration user msp ##
```
export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/clients/admin
./fabric-ca-client register --id.name Admin --id.secret adminpw --id.type user --id.affiliation supplier
export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/users/Admin
./fabric-ca-client enroll -u http://Admin:adminpw@localhost:7054
```

## Extract the ca certificates ##
```
export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca-platform/client/certs
./fabric-ca-client getcacert -u http://admin:adminpw@localhost:7054
```

## Structuring the files for network generation ##
The CA does not produce the files with the naming or folder convention that the configtxgen tool requires from the configuration in the configtx.yaml and docker-compose.yml files. The next set of commands move and rename files into the require folder structure.

The crypto-config tree structure for the supplier will be:
```
crypto-config
    + peerOrganizations
	    + supplier.com
			+ msp 
			    + admincerts
				+ cacerts
			+ peers
			    + peer0.supplier.com
				    + msp
						+ admincerts
						+ cacerts
						+ keystore
						+ signcerts
				+ peer1.supplier.com
				    + msp
						+ admincerts
						+ cacerts
						+ keystore
						+ signcerts
			+ users
			    + Admin@supplier.com
				    + msp
					    + admincerts
						+ cacerts
						+ keystore
						+ signcerts
```

The commands to create and populate the peers:
```
mkdir -p ~/crypto-config/peerOrganizations/supplier.com

mkdir -p ~/crypto-config/peerOrganizations/supplier.com/peers/peer0.supplier.com
cp -a ~/fabric-ca-platform/peers/peer0/msp ~/crypto-config/peerOrganizations/supplier.com/peers/peer0.supplier.com/
mv ~/crypto-config/peerOrganizations/supplier.com/peers/peer0.supplier.com/msp/cacerts/localhost-7054.pem ~/crypto-config/peerOrganizations/supplier.com/peers/peer0.supplier.com/msp/cacerts/ca.supplier.com-cert.pem
mv ~/crypto-config/peerOrganizations/supplier.com/peers/peer0.supplier.com/msp/signcerts/cert.pem ~/crypto-config/peerOrganizations/supplier.com/peers/peer0.supplier.com/msp/signcerts/peer0.supplier.com-cert.pem
mkdir -p ~/crypto-config/peerOrganizations/supplier.com/peers/peer0.supplier.com/msp/admincerts
cp ~/fabric-ca-platform/users/Admin/msp/signcerts/cert.pem ~/crypto-config/peerOrganizations/supplier.com/peers/peer0.supplier.com/msp/admincerts/Admin@supplier.com-cert.pem

mkdir -p ~/crypto-config/peerOrganizations/supplier.com/peers/peer1.supplier.com
cp -a ~/fabric-ca-platform/peers/peer1/msp ~/crypto-config/peerOrganizations/supplier.com/peers/peer1.supplier.com/
mv ~/crypto-config/peerOrganizations/supplier.com/peers/peer1.supplier.com/msp/cacerts/localhost-7054.pem ~/crypto-config/peerOrganizations/supplier.com/peers/peer1.supplier.com/msp/cacerts/ca.supplier.com-cert.pem
mv ~/crypto-config/peerOrganizations/supplier.com/peers/peer1.supplier.com/msp/signcerts/cert.pem ~/crypto-config/peerOrganizations/supplier.com/peers/peer1.supplier.com/msp/signcerts/peer1.supplier.com-cert.pem
mkdir -p ~/crypto-config/peerOrganizations/supplier.com/peers/peer1.supplier.com/msp/admincerts
cp ~/fabric-ca-platform/users/Admin/msp/signcerts/cert.pem ~/crypto-config/peerOrganizations/supplier.com/peers/peer1.supplier.com/msp/admincerts/Admin@supplier.com-cert.pem

cp -a ~/fabric-ca-platform/users ~/crypto-config/peerOrganizations/supplier.com/
mv ~/crypto-config/peerOrganizations/supplier.com/users/Admin/msp/cacerts/localhost-7054.pem ~/crypto-config/peerOrganizations/supplier.com/users/Admin/msp/cacerts/ca.supplier.com-cert.pem
mv ~/crypto-config/peerOrganizations/supplier.com/users/Admin/msp/signcerts/cert.pem ~/crypto-config/peerOrganizations/supplier.com/users/Admin/msp/signcerts/Admin@supplier.com-cert.pem
mkdir -p ~/crypto-config/peerOrganizations/supplier.com/users/Admin/msp/admincerts
cp ~/crypto-config/peerOrganizations/supplier.com/users/Admin/msp/signcerts/Admin@supplier.com-cert.pem ~/crypto-config/peerOrganizations/supplier.com/users/Admin/msp/admincerts/Admin@supplier.com-cert.pem

cp -a ~/fabric-ca-platform/client/certs/msp ~/crypto-config/peerOrganizations/supplier.com/
mkdir -p ~/crypto-config/peerOrganizations/supplier.com/msp/admincerts
cp ~/crypto-config/peerOrganizations/supplier.com/users/Admin/msp/admincerts/Admin@supplier.com-cert.pem ~/crypto-config/peerOrganizations/supplier.com/msp/admincerts
mv ~/crypto-config/peerOrganizations/supplier.com/msp/cacerts/localhost-7054.pem ~/crypto-config/peerOrganizations/supplier.com/msp/cacerts/ca.supplier.com-cert.pem

```

## Troubleshooting ##
https://gerrit.hyperledger.org/r/#/c/10871/6/examples/e2e_cli/fabric-ca-cryptogen.sh