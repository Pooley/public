The composer-client project is a Hyperledger Composer client application for the sibling composer project.

To run
```
$ npm install
$ npm run composer-client
```