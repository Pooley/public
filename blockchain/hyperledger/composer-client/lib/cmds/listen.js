#!/usr/bin/env node

'use strict';

const ComposerClient = require('./../composerclient.js');
const winston = require('winston');
const LOG = winston.loggers.get('application');

exports.command = 'listen';
exports.desc = 'Listen for all events';
exports.builder = {};
exports.handler = async function (argv) {

    try {
        await ComposerClient.listen(argv);
        LOG.info('Command completed successfully.');
    } catch(error) {
      LOG.error(error+ '\nCommand failed.');
    }
};

