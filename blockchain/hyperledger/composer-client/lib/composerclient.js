const BusinessNetworkConnection = require('composer-client').BusinessNetworkConnection;
const Table = require('cli-table');
const winston = require('winston');
const prettyjson = require('prettyjson');

// these are the credentials to use to connect to the Hyperledger Fabric
let cardname = 'admin@composer-bna';

const LOG = winston.loggers.get('application');

class ComposerClient {
	constructor() {
        this.bizNetworkConnection = new BusinessNetworkConnection();
    }
	
	async init() {
        this.businessNetworkDefinition = await this.bizNetworkConnection.connect(cardname);
		LOG.info('ComposerClient:<init>', 'businessNetworkDefinition obtained', this.businessNetworkDefinition.getIdentifier());
    }
	
    async listen() {
        this.bizNetworkConnection.on('event', (event) => {
          if (event.$type == 'SendInvoiceEvent') {
            LOG.info('Received SendInvoiceEvent for invoice ' + event.invoice);
          }
        });
    }

    static async listen(args) {
        let cc = new ComposerClient();
        await cc.init();
        await cc.listen();
    }
}

module.exports = ComposerClient;

