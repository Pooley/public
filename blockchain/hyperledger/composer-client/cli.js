'use strict';

const winston = require('winston');

process.on('uncaughtException', function (err) {
    console.log( 'Uncaught Exception: ' + err.stack);
});

winston.loggers.add('application', {
    console: {
        level: 'silly',
        colorize: true,
        label: 'ComposerClient-App'
    }
});

const LOG = winston.loggers.get('application');

LOG.info('Hyperledger Composer: composerclient application');
try {
    require('yargs')
    .usage ('node cli.js  <participant> <action>')
    .commandDir('lib/cmds')
    .demand(1,'Please specify a partipant, for example:  composerclient <action>')
    .help()
    .strict()
    .recommendCommands()
    .argv;
} catch (err) {
  console.log(err);
}

