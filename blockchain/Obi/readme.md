## Obi

Obi ties blockchains together. Assets can be moved from one to another, transactions initiated and the full chain of an asset’s history retained. Obi is itself a blockchain, decentralised, immutable and trustworthy by consensus.

### Why is Obi necessary
Integration is unavoidable and necessary. Bitcoin cannot be used on Ethereum; Walmart’s Food Trust is separate from Starbuck’s Coffee trust and they both may be running Hyperledger Fabric. R3’s Corda may be used by a consumer to buy both coffee and groceries. Without the ability to connect the segments of blocks as data and assets cross chains, the promise of history is lost.


### How is Obi different
There are other blockchain interopability initiatives. However, their goals are more directed towards being a blockchain platform first and controlling a protocol than being a pure piece of middleware. Obi's aim is simple: allowing different chains to interact and maintaining the block tracability of an asset accross chains.

### How Obi ties blockchains
New blocks store the hash of the previous block. Distributed ledgers prove the correct hash and identify corrupted ledger copies. Obi uses the hashes of the two chains to tie them together by referencing the originating chain’s new hash in the destination chain’s new block by the transaction hashes that were generated on both chains.

Obi in its own blockchain keeps a record of all cross-chain transactions in its blocks. Obi has the block hashes of both blockchains and creates a transaction with all the information. Thus, Obi acts as an independant, yet still trustworthy by distribution, record of cross-blockchain activity.

Now let’s continue A’s journey and it crosses into a third blockchain. The first originating chain has no knowledge that this took place at all. As the recorder, Obi has the records of both chain jumps and can reconstruct a singular view of A throughout its lifecycle regardless of how many chains it crossed even when they re-enter chains with earlier history.

By acting only as the recorder of cross-chain transactions Obi minimises the amount of data to be stored, even the data of the transaction as that is unimportant, only the record of passing. Obi remains minimalist.

### What kind of blockchain is Obi
Obi ties any blockchain together. It’s permission is private. Only the parties that have permission to view or execute the cross-chain in their blockchains can see the binding transactions in Obi.

- The identity originating the transaction has to be present on the originating and destination blockchain.
- The receiving identity need only be known on the destination blockchain.
- The originating identity must be known to Obi.

